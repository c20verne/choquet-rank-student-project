package io.gitlab.chaver.minimax.rules.io;

import io.gitlab.chaver.minimax.io.IAlternative;
import io.gitlab.chaver.mining.rules.io.IRule;
import lombok.Getter;
import lombok.Setter;

import java.util.HashMap;
import java.util.Map;

@Getter
public class RuleWithMeasures implements IRule {

    private int[] x;
    private int[] y;
    private Map<String, Double> measureValues;
    private @Setter Double score;
    private int freqX;
    private int freqZ;
    private int freqY;

    public RuleWithMeasures(IRule r, IAlternative a, String[] measures) {
        x = r.getX();
        y = r.getY();
        freqX = r.getFreqX();
        freqY = r.getFreqY();
        freqZ = r.getFreqZ();
        measureValues = new HashMap<>();
        for (int i = 0; i < measures.length; i++) {
            measureValues.put(measures[i], a.getVector()[i]);
        }
    }

    public RuleWithMeasures(RuleWithMeasures copy, boolean withScore) {
        x = copy.getX();
        y = copy.getY();
        freqX = copy.getFreqX();
        freqY = copy.getFreqY();
        freqZ = copy.getFreqZ();
        measureValues = copy.getMeasureValues();
        score = withScore ? copy.score : null;
    }
}
