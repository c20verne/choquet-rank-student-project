package io.gitlab.chaver.minimax.cli;

import com.google.gson.Gson;
import io.gitlab.chaver.minimax.io.Alternative;
import io.gitlab.chaver.minimax.io.IAlternative;
import io.gitlab.chaver.minimax.normalizer.INormalizer;
import io.gitlab.chaver.minimax.rules.io.RuleMeasures;
import io.gitlab.chaver.minimax.rules.io.RuleWithMeasures;
import io.gitlab.chaver.minimax.util.RandomUtil;
import io.gitlab.chaver.mining.rules.io.IRule;
import picocli.CommandLine;
import picocli.CommandLine.Option;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.stream.Collectors;

import static io.gitlab.chaver.minimax.learn.train.LearnUtil.*;

public class SplitTrainingTestCli implements Callable<Integer> {

    @Option(names = "-d", description = "Path of the rules data", required = true)
    private String dataPath;
    @Option(names = "--train", description = "%% of data used for the training (value in [0,1])", required = true)
    private double trainingPercentage;
    @Option(names = "-r", description = "Path of the result files", required = true)
    private String resPath;
    @Option(names = "-m", description = "Measures to compute for each rule", required = true, split = ":")
    private String[] measures;
    @Option(names = "--seed", description = "Seed for random number generation")
    private long seed = 2994274L;
    @Option(names = "--smooth", description = "Smooth measures")
    private double smoothCounts = 0.1d;

    private <T> void writeJsonLines(List<T> l, String path) throws IOException {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(path))) {
            Gson gson = new Gson();
            for (T elt : l) {
                writer.write(gson.toJson(elt) + "\n");
            }
        }
    }

    @Override
    public Integer call() throws Exception {
        List<IRule> rules = readAssociationRules(dataPath + "_sols.jsonl");
        int nbTransactions = getNbTransactions(dataPath + "_prop.jsonl");
        List<IAlternative> alternatives = rules
                .stream()
                .map(r -> new Alternative(new RuleMeasures(r, nbTransactions, smoothCounts).computeMeasures(measures)))
                .collect(Collectors.toCollection(ArrayList::new));
        alternatives = INormalizer.tchebychefNormalize(alternatives);
        RandomUtil.getInstance().setSeed(seed);
        int foldSize = (int) (trainingPercentage * rules.size());
        int[][] folds = RandomUtil.getInstance().kFolds(1, rules.size(), foldSize);
        List<IAlternative> trainingAlternatives = getTrainingData(alternatives, folds).get(0);
        List<IRule> trainingRules = getTrainingData(rules, folds).get(0);
        List<IAlternative> testAlternatives = getTestData(alternatives, folds).get(0);
        List<IRule> testRules = getTestData(rules, folds).get(0);
        List<RuleWithMeasures> trainingRuleWithMeasures = new ArrayList<>();
        for (int i = 0; i < trainingRules.size(); i++) {
            RuleWithMeasures r = new RuleWithMeasures(trainingRules.get(i), trainingAlternatives.get(i), measures);
            trainingRuleWithMeasures.add(r);
        }
        writeJsonLines(trainingRuleWithMeasures, resPath + "_train.jsonl");
        List<RuleWithMeasures> testRuleWithMeasures = new ArrayList<>();
        for (int i = 0; i < testRules.size(); i++) {
            RuleWithMeasures r = new RuleWithMeasures(testRules.get(i), testAlternatives.get(i), measures);
            testRuleWithMeasures.add(r);
        }
        writeJsonLines(testRuleWithMeasures, resPath + "_test.jsonl");
        return 0;
    }

    public static void main(String[] args) {
        int exitCode = new CommandLine(new SplitTrainingTestCli()).execute(args);
        System.exit(exitCode);
    }
}
