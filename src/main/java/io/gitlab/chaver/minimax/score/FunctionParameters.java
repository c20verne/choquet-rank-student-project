package io.gitlab.chaver.minimax.score;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.HashSet;
import java.util.Set;

import static io.gitlab.chaver.minimax.util.BitSetUtil.intToBitSet;
import static io.gitlab.chaver.minimax.util.BitSetUtil.isLexBefore;

/**
 * Class to save the parameters of a score function in a file
 */
@Getter
@Setter
@ToString
public class FunctionParameters {

    private String functionType;
    private double[] weights;
    private int kAdditivity;
    private int nbCriteria;
    private double timeToLearn;
    private boolean timeOut;
    private String[] errorMessages;
    private int nbIterations;
    private double[] shapleyValues;
    private double[][] interactionIndices;
    private double[] obj;
    private String[] weightLabels;
    private String[] measureNames;

    private BitSet[] orderCapacitySets() {
        int maxNbCapacitySets = (int) Math.pow(2, nbCriteria);
        Set<BitSet> selectedCapacitySets = new HashSet<>();
        for (int i = 0; i < maxNbCapacitySets; i++) {
            BitSet capacitySet = intToBitSet(i, nbCriteria);
            if (capacitySet.cardinality() <= kAdditivity) {
                selectedCapacitySets.add(capacitySet);
            }
        }
        return new ArrayList<>(selectedCapacitySets).stream().sorted((o1, o2) -> {
            if (o1.cardinality() < o2.cardinality()) {
                return -1;
            }
            if (o2.cardinality() < o1.cardinality()) {
                return 1;
            }
            if (isLexBefore(o1, o2, nbCriteria)) {
                return -1;
            }
            if (isLexBefore(o2, o1, nbCriteria)) {
                return 1;
            }
            return 0;
        }).toArray(BitSet[]::new);
    }

    private String bitSetToString(BitSet b, String[] measureNames) {
        StringBuilder str = new StringBuilder("{");
        boolean comma = false;
        for (int i = 0; i < measureNames.length; i++) {
            if (b.get(i)) {
                if (comma) str.append(",");
                str.append(measureNames[i]);
                comma = true;
            }
        }
        str.append("}");
        return str.toString();
    }

    public void addWeightLabels(String[] measureNames) {
        if (functionType.equals(ChoquetMobiusScoreFunction.TYPE)) {
            BitSet[] capacity = orderCapacitySets();
            if (capacity.length != weights.length) {
                throw new RuntimeException("Capacity and weights must have the same length");
            }
            weightLabels = new String[capacity.length];
            for (int i = 0; i < capacity.length; i++) {
                weightLabels[i] = bitSetToString(capacity[i], measureNames);
            }
            return;
        }
        if (functionType.equals(LinearScoreFunction.TYPE)) {
            weightLabels = new String[measureNames.length];
            for (int i = 0; i < measureNames.length; i++) {
                weightLabels[i] = "{" + measureNames[i] + "}";
            }
            return;
        }
        throw new RuntimeException("This function type is not implemented for labels : " + functionType);
    }
}
