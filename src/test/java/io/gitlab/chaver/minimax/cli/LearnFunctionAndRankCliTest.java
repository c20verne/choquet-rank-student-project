package io.gitlab.chaver.minimax.cli;

import org.junit.jupiter.api.Test;
import picocli.CommandLine;

import java.io.File;

import static org.junit.jupiter.api.Assertions.*;

class LearnFunctionAndRankCliTest {

    @Test
    void test() throws Exception {
        String rulesPath = "results/rules/iris";
        String trainingPercentage = "0.26";
        String resPath = File.createTempFile("rules", "").getAbsolutePath();
        String measures = "phi:kruskal:yuleQ";
        String seed = "1234";
        String[] args = {"-d", rulesPath, "--train", trainingPercentage, "-r", resPath, "-m", measures,
                "--seed", seed};
        new CommandLine(new SplitTrainingTestCli()).execute(args);
        String oracleName = "owa";
        String learningAlgorithm = "kappalab";
        String learnResPath = File.createTempFile("learn", "").getAbsolutePath();
        args = new String[]{"-d", rulesPath, "-r", learnResPath, "-m", measures,
                "--seed", seed, "-o", oracleName, "-l", learningAlgorithm, "--tt", resPath};
        int exitCode = new CommandLine(new LearnFunctionAndRankCli()).execute(args);
        assertEquals(0, exitCode);
    }

    @Test
    void test2() throws Exception {
        String rulesPath = "results/rules/iris";
        String trainingPercentage = "0.26";
        String resPath = File.createTempFile("rules", "").getAbsolutePath();
        String measures = "phi:kruskal:yuleQ";
        String seed = "1234";
        String[] args = {"-d", rulesPath, "--train", trainingPercentage, "-r", resPath, "-m", measures,
                "--seed", seed};
        new CommandLine(new SplitTrainingTestCli()).execute(args);
        String oracleName = "chiSquared";
        String learningAlgorithm = "kappalab";
        String learnResPath = File.createTempFile("learn", "").getAbsolutePath();
        args = new String[]{"-d", rulesPath, "-r", learnResPath, "-m", measures,
                "--seed", seed, "-o", oracleName, "-l", learningAlgorithm, "--tt", resPath};
        int exitCode = new CommandLine(new LearnFunctionAndRankCli()).execute(args);
        assertEquals(0, exitCode);
    }

    @Test
    void test3() throws Exception {
        String rulesPath = "results/rules/iris";
        String trainingPercentage = "0.26";
        String resPath = File.createTempFile("rules", "").getAbsolutePath();
        String measures = "phi:kruskal:yuleQ";
        String seed = "1234";
        String[] args = {"-d", rulesPath, "--train", trainingPercentage, "-r", resPath, "-m", measures,
                "--seed", seed};
        new CommandLine(new SplitTrainingTestCli()).execute(args);
        String oracleName = "choquetPearson";
        String learningAlgorithm = "kappalab";
        String learnResPath = File.createTempFile("learn", "").getAbsolutePath();
        args = new String[]{"-d", rulesPath, "-r", learnResPath, "-m", measures,
                "--seed", seed, "-o", oracleName, "-l", learningAlgorithm, "--tt", resPath};
        int exitCode = new CommandLine(new LearnFunctionAndRankCli()).execute(args);
        assertEquals(0, exitCode);
    }

}