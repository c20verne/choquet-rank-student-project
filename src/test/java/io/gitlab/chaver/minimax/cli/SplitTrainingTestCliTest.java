package io.gitlab.chaver.minimax.cli;

import org.junit.jupiter.api.Test;
import picocli.CommandLine;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import static org.junit.jupiter.api.Assertions.*;

class SplitTrainingTestCliTest {

    private long countLines(String filePath) throws IOException {
        return Files.readAllLines(Paths.get(filePath)).stream().count();
    }

    @Test
    void test() throws Exception {
        String rulesPath = "results/rules/iris";
        String trainingPercentage = "0.26";
        String resPath = File.createTempFile("rules", "").getAbsolutePath();
        String measures = "phi:kruskal:yuleQ";
        String seed = "1234";
        String[] args = {"-d", rulesPath, "--train", trainingPercentage, "-r", resPath, "-m", measures,
        "--seed", seed};
        int exitCode = new CommandLine(new SplitTrainingTestCli()).execute(args);
        assertEquals(0, exitCode);
        assertEquals(51, countLines(resPath + "_train.jsonl"));
        assertEquals(147, countLines(resPath + "_test.jsonl"));
    }

}