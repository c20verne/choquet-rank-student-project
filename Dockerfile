FROM ubuntu:latest

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update && apt-get install -y r-base wget gzip
RUN R -e "install.packages(c('kappalab', 'jsonlite'), repos='http://cran.rstudio.com/')"
RUN wget https://download.joachims.org/svm_rank/current/svm_rank_linux64.tar.gz
RUN mkdir choquet-rank
WORKDIR choquet-rank
RUN mkdir svm_rank_linux64
WORKDIR svm_rank_linux64
RUN mv ../../svm_rank_linux64.tar.gz .
RUN tar -xzf svm_rank_linux64.tar.gz
WORKDIR ..
RUN apt-get update && apt-get install -y maven
COPY data/ ./data
COPY results/ ./results
COPY scripts/ ./scripts
COPY src/ ./src
COPY pom.xml/ .
COPY Makefile .
RUN mkdir RankLib
COPY RankLib/RankLib-2.18.jar RankLib/RankLib-2.18.jar
RUN mvn package
ENTRYPOINT ["bash"]